
const express = require ('express')
const app = express()
const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.get('/',(request,response) => {
  response.end('Hello World')
})

app.get('/hello',(request,response) => {
  response.send('Hello from/hello endpoint!')
})

app.put('/change-password',(request,response) => {
  let message

  for(let i=0;i<users.length;i++){
    if(request.body.username == users[i].username){
      users[i].password == request.body.password
      message = `User ${request.body.username}'s password has been updated`

    break
    } else{
      message = 'User does not exist.'
    }
  }
  response.send(message)
})
// mock database
let users = [];

app.post('/register',(request,response) => {

  if(request.body.username !== '' && request.body.password !== ''){
    users.push(request.body)
    console.log(users)
    response.send(`User ${request.body.username} has been successfully registered`)
  }else{
    response.send('Please input BOTH username and password')
  }
})



app.get('/home',(request,response) => {
  response.send('this is the home page')
})

app.get('/get-user',(request,response) => {
  response.send(users)
})

app.delete('/delete-user',(request,response) => {

  let display

  for(let i=0;i<users.length;i++){
    if(request.body.username == users[i].username){
       
      users.pop(users[i].username);
      display = `User ${request.body.username}'s  has been deleted`

      break
    } else{
      display = 'User does not exist.'
    }
  }
  response.send(display)
  })


app.listen(port,() => console.log(`Server is running at port ${port}`))